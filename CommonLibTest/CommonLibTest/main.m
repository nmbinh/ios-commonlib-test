//
//  main.m
//  CommonLibTest
//
//  Created by Binh Nguyen on 6/15/15.
//  Copyright (c) 2015 Binh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
